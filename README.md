Projeto para estágio pós-doutoral na Universidade Federal de Alagoas ([UFAL]
(http://www.ufal.edu.br/)), Brasil.

Projeto a ser submetido a terceira chamada do ano de 2016 do programa de
bolsas de pesquisa no país (Brasil), na modalidade Pós-Doutorado Júnior
([PDJ](http://cnpq.br/web/guest/view/-/journal_content/56_INSTANCE_0oED/10157/2958271?COMPANY_ID=10132#PDJ)),
do Conselho Nacional de Desenvolvimento Científico e Tecnológico
([CNPq](http://cnpq.br)), uma agência do Ministério da Ciência, Tecnologia e
Inovação do governo brasileiro ([MCTI/BR](http://www.mcti.gov.br/)).